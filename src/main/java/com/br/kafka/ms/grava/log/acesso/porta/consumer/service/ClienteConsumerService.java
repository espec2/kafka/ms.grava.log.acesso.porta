package com.br.kafka.ms.grava.log.acesso.porta.consumer.service;

import com.br.kafka.msvalidaporta.producer.Cliente;
import com.opencsv.CSVWriter;
import org.springframework.stereotype.Service;

import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ClienteConsumerService {

    private String pathToCsv = "/home/a2/Documentos/Itau/kafkateste.csv";

    public void gerarCsv(Cliente cliente) throws IOException {
        File csvFile = new File(pathToCsv);
        List<String[]> registrosCsv = new ArrayList<String[]>();

        if (csvFile.isFile()) {
            registrosCsv = lerCsv();
        }else{
            registrosCsv.add(new String[] { "Cliente" , "Porta" , "TemAcessoPorta" });
        }
        registrosCsv.add(new String[] { cliente.getCliente(), String.valueOf(cliente.getPorta()), String.valueOf(cliente.isTemAcessoPorta())});
        escreverCsv(registrosCsv);
    }

    private List<String[]> lerCsv() throws IOException {
        BufferedReader csvReader = new BufferedReader(new FileReader(pathToCsv));
        String row;
        List<String[]> data = new ArrayList<String[]>();
        while ((row = csvReader.readLine()) != null) {
            data.add(row.split(";"));
        }
        csvReader.close();
        return data;
    }

    private void escreverCsv(List<String[]> registrosCsv){
        File file = new File(pathToCsv);

        try {
            // create FileWriter object with file as parameter
            FileWriter outputfile = new FileWriter(file);

            // create CSVWriter with '|' as separator
            CSVWriter writer = new CSVWriter(outputfile, ';',
                    CSVWriter.NO_QUOTE_CHARACTER,
                    CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                    CSVWriter.DEFAULT_LINE_END);

            writer.writeAll(registrosCsv);

            // closing writer connection
            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }


}
