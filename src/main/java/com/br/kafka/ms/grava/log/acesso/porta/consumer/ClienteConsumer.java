package com.br.kafka.ms.grava.log.acesso.porta.consumer;

import com.br.kafka.ms.grava.log.acesso.porta.consumer.service.ClienteConsumerService;
import com.br.kafka.msvalidaporta.producer.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class ClienteConsumer {
    @Autowired
    private ClienteConsumerService clienteConsumerService;

    @KafkaListener(topics = "spec2-andre-vinicius-1", groupId = "41343874858")
    public void receber(@Payload Cliente cliente) throws IOException {
        System.out.println(cliente.getCliente() + "," + cliente.getPorta() + "," + cliente.isTemAcessoPorta());
        clienteConsumerService.gerarCsv(cliente);
    }

}
